#!/bin/python3

"""
        DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                    Version 2, December 2004

 Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

 Everyone is permitted to copy and distribute verbatim or modified
 copies of this license document, and changing it is allowed as long
 as the name is changed.

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  0. You just DO WHAT THE FUCK YOU WANT TO.
"""

from os.path import exists
import os
import json
import datetime
import time
import random

__COLLECTION_FILE_NAME__ = "collection.json"
__SCORE_SAMPLE_SIZE__ = 10
__OLD_COUNT__ = 10
__INACCURATE_COUNT__ = 5
__SLOW_COUNT__ = 5
__TABLE_SIZE__ = 20

def assign_to_collection(collection, problem, solution):
    collection.append({
        'problem': problem,
        'solution': solution,
        'accuracy': [0 for i in range(__SCORE_SAMPLE_SIZE__)],
        'speed': [0 for i in range(__SCORE_SAMPLE_SIZE__)],
        'last_practiced': time.mktime(datetime.datetime.now().timetuple())
        })

def add_full_table_to_collection(size, collection, problem_function, assignment_function):
    for i in range(size):
        for j in range(size):
            problem, solution = problem_function(i + 1, j + 1)
            assignment_function(collection, problem, solution)

def add_half_table_to_collection(size, collection, problem_function, assignment_function):
    for i in range(size):
        for j in range(size):
            if i < j:
                continue
            else:
                problem, solution = problem_function(i + 1, j + 1)
                assignment_function(collection, problem, solution)

def mult_problem(a, b):
    return f"{a} x {b}", f"{a*b}"

def add_problem(a, b):
    return f"{a} + {b}", f"{a+b}"

def sub_problem(a, b):
    return f"{a} - {b}", f"{a-b}"

def div_problem(a, b):
    remainder = f"r{a%b}" if a%b else ""
    return f"{a} / {b}", f"{a//b}{remainder}"

def initialize_collection():
    collection = []
    add_half_table_to_collection(__TABLE_SIZE__, collection, add_problem, assign_to_collection)
    add_half_table_to_collection(__TABLE_SIZE__, collection, sub_problem, assign_to_collection)
    add_half_table_to_collection(__TABLE_SIZE__, collection, mult_problem, assign_to_collection)
    add_half_table_to_collection(__TABLE_SIZE__, collection, div_problem, assign_to_collection)
    collection_json = convert_to_json(collection)
    write_to_json_file(collection_json);

def read_from_json_file():
    with open(__COLLECTION_FILE_NAME__) as collection_file:
        collection = json.load(collection_file)
    return collection

def convert_to_json(collection):
    return json.dumps(collection, indent=2)

def write_to_json_file(collection_json):
    with open(__COLLECTION_FILE_NAME__, "w") as outfile:
        outfile.write(collection_json)

def get_slow_problems(collection):
    return sorted(collection,
            key = lambda x: sum(x['speed']) / len(x['speed']),
            reverse=True
            )[:__SLOW_COUNT__]

def get_inaccurate_problems(collection):
    return sorted(collection,
            key = lambda x: sum(x['accuracy']) / len(x['accuracy'])
            )[:__INACCURATE_COUNT__]

def get_old_problems(collection):
    return sorted(collection, key = lambda x: x['last_practiced'])[:__OLD_COUNT__]

def get_scheduled_problems(collection):
    old_problems = get_old_problems(collection)
    inaccurate_problems = get_inaccurate_problems(collection)
    slow_problems = get_slow_problems(collection)
    return [*old_problems, *inaccurate_problems, *slow_problems]

def find_by_problem(collection, problem_string):
    for card in collection:
        if card['problem'] == problem_string:
            return card

def roll_on_value(array, value):
    array.append(value)
    return array[1:]

def score_problem(score, elapsed_time, collection, current_problem):
    card = find_by_problem(collection, current_problem['problem'])
    card['speed'] = roll_on_value(card['speed'], elapsed_time.total_seconds())
    card['accuracy'] = roll_on_value(card['accuracy'], score)
    card['last_practiced'] = time.mktime(datetime.datetime.now().timetuple())

def review_incorrect(current_problem):
        print("Wrong!")

        while True:
            print("Please type in correct answer:")
            print(f"\"{current_problem['solution']}\"")
            if current_problem['solution'] == input():
                break
            else:
                continue

def game_loop(collection):
    message = ""
    scheduled_problems = []

    while True:
        os.system('cls' if os.name == 'nt' else 'clear')
        print(message)
        if len(scheduled_problems) == 0:
            scheduled_problems = get_scheduled_problems(collection)
            random.shuffle(scheduled_problems)
        current_problem = scheduled_problems.pop()
        start_time = datetime.datetime.now()
        answer = input(f"{current_problem['problem']}: ")
        elapsed_time = datetime.datetime.now() - start_time
        if answer == current_problem["solution"]:
            message = "CORRECT!"
            score_problem(1, elapsed_time, collection, current_problem)
        else:
            review_incorrect(current_problem)
            score_problem(0, elapsed_time, collection, current_problem)
        collection_json = convert_to_json(collection)
        write_to_json_file(collection_json)

if __name__ == "__main__":
    if not exists(__COLLECTION_FILE_NAME__):
        initialize_collection()
    collection = read_from_json_file()
    try:
        game_loop(collection)
    except KeyboardInterrupt:
        print("\nCTRL+C detected. Goodbye!"),
        exit()
